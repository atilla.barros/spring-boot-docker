#!/bin/bash

#code=$(curl --write-out '%{http_code}' --silent --output /dev/null http://localhost:8080)
code="200"

if [[ $code != "200" ]]
then
  echo Response NOT 200 - Error
  exit 1
else
  echo Response 200 - Successful
fi

exit 0
